#!/usr/bin/env python
"""
  BQ24450/UC3906 dual stage battery charger modeling tool
 
  Sliders are used to set resistor values and transistor saturation voltages
  and circuit parameters are computed.
  The equations are from the BQ24450 and UC3906 data sheets and the
  UC3906 U104 App Note.

  pete@soper.us June 2016
"""
__author__ = "Pete Soper"
__license__ = "MIT"
__version__ = "0.91"

##########
## BEGIN Resistor utilities
##########
"""
A python script to calculate E96 resistor values

Originally published at techoverflow.net
"""
import itertools
import math

#__author__ = "Uli Koehler"
#__license__ = "CC0 1.0 Universal"
#__version__ = "1.1"

# Standard resistor sequences
e96 = [1.00, 1.02, 1.05, 1.07, 1.10, 1.13, 1.15, 1.18, 1.21, 1.24, 1.27, 1.30, 1.33, 1.37, 1.40,
       1.43, 1.47, 1.50, 1.54, 1.58, 1.62, 1.65, 1.69, 1.74, 1.78, 1.82, 1.87, 1.91, 1.96, 2.00,
       2.05, 2.10, 2.15, 2.21, 2.26, 2.32, 2.37, 2.43, 2.49, 2.55, 2.61, 2.67, 2.74, 2.80, 2.87,
       2.94, 3.01, 3.09, 3.16, 3.24, 3.32, 3.40, 3.48, 3.57, 3.65, 3.74, 3.83, 3.92, 4.02, 4.12,
       4.22, 4.32, 4.42, 4.53, 4.64, 4.75, 4.87, 4.99, 5.11, 5.23, 5.36, 5.49, 5.62, 5.76, 5.90,
       6.04, 6.19, 6.34, 6.49, 6.65, 6.81, 6.98, 7.15, 7.32, 7.50, 7.68, 7.87, 8.06, 8.25, 8.45,
       8.66, 8.87, 9.09, 9.31, 9.53, 9.76]
e48 = [1.00, 1.05, 1.10, 1.15, 1.21, 1.27, 1.33, 1.40, 1.47, 1.54, 1.62, 1.69,
       1.78, 1.87, 1.96, 2.05, 2.15, 2.26, 2.37, 2.49, 2.61, 2.74, 2.87, 3.01,
       3.16, 3.32, 3.48, 3.65, 3.83, 4.02, 4.22, 4.42, 4.64, 4.87, 5.11, 5.36,
       5.62, 5.90, 6.19, 6.49, 6.81, 7.15, 7.50, 7.87, 8.25, 8.66, 9.09, 9.53]
e24 = [1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.7, 3.0,
       3.3, 3.6, 3.9, 4.3, 4.7, 5.1, 5.6, 6.2, 6.8, 7.5, 8.2, 9.1]
e12 = [1.0, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2]


def getResistorRange(multiplicator, sequence=e96):
    "Get a single E96 range of resistors, e.g. for 1k use multiplicator = 1000"
    return [val*multiplicator for val in sequence]

def getStandardResistors(minExp=-1, maxExp=9, sequence=e96):
    "Get a list of all standard resistor values from 100mOhm up to 976 MOhm in Ohms"
    exponents = list(itertools.islice(itertools.count(minExp, 1), 0, maxExp - minExp))
    multiplicators = [10**x for x in exponents]
    return itertools.chain(*[getResistorRange(r, sequence=sequence) for r in multiplicators])

def findNearestResistor(value, sequence=e96):
    "Find the standard reistor value with the minimal difference to the given value"
    return min(getStandardResistors(sequence=sequence), key=lambda r: abs(value - r))

def _formatWithSuffix(v, suffix):
    """
    Format a given value with a given suffix.
    This helper function formats the value to 3 visible digits.
    v must be pre-multiplied by the factor implied by the suffix

    >>> _formatWithSuffix(1.01, "A")
    '1.01 A'
    >>> _formatWithSuffix(1, "A")
    '1.00 A'
    >>> _formatWithSuffix(101, "A")
    '101 A'
    >>> _formatWithSuffix(99.9, "A")
    '99.9 A'
    """
    if v < 10:
        res = "%.2f" % v
    elif v < 100:
        res = "%.1f" % v
    else:  # Should only happen if v < 1000
        res = "%d" % int(v)
    #Avoid appending whitespace if there is no suffix
    if suffix:
        return "%s %s" % (res, suffix)
    else:
        return res

def formatValue(v, unit=""):
    """
    Format v using SI suffices with optional units.
    Produces a string with 3 visible digits.

    >>> formatValue(1.0e-15, "V")
    '1.00 fV'
    >>> formatValue(234.6789e-3, "V")
    '234 mV'
    >>> formatValue(234.6789, "V")
    '234 V'
    >>> formatValue(2345.6789, "V")
    '2.35 kV'
    >>> formatValue(2345.6789e6, "V")
    '2.35 GV'
    >>> formatValue(2345.6789e12, "V")
    '2.35 EV'
    """
    suffixMap = {
        -5: "f", -4: "p", -3: "n", -2: "N<", -1: "m",
        0: "", 1: "k", 2: "M", 3: "G", 4: "T", 5: "E"
    }
    #Suffix map is indexed by one third of the decadic logarithm.
    exp = 0 if v == 0.0 else math.log(v, 10.0)
    suffixMapIdx = int(math.floor(exp / 3.0))
    #Ensure we're in range
    if suffixMapIdx < -5:
        suffixMapIdx = -5
    elif suffixMapIdx > 5:
        suffixMapIdx = 5
    #Pre-multiply the value
    v = v * (10.0 ** -(suffixMapIdx * 3))
    #Delegate the rest of the task to the helper
    return _formatWithSuffix(v, suffixMap[suffixMapIdx] + unit)


def formatResistorValue(v):
    return formatValue(v, unit="ohm")

# Usage example: Find and print the E48 resistor closest to 5 kOhm
#if __name__ == "__main__":
#    import doctest
#    doctest.testmod()
#    print(formatResistorValue(findNearestResistor(5000, sequence=e48)))
##########
## END Resistor
##########
##########
## BEGIN Modeler
##########
from Tkinter import *
from datetime import datetime, date, time

root = Tk()

scale_length = 600

# These are the offsets from ground as measured with a test circuit
# operating at a default Vf of 13.65v, Voc of 14.47, Vt of 10.5, Id of 55uA with the
# default exact resistor values below

Vpsat = 0.028
Vssat = 0.003

# Exact resistor values to start

Tolerance_percent = 0
Tolerance_sequence = None

Ra = 211250
Rb = 19234
Rc = 46369
Rd = 555880
Rs = 0.167

Vref = 2.3

# make sure these are defined

Voc = 0
Vf = 0
Vt = 0
Vt = 0
V12 = 0
V31 = 0
Ioct = 0
Imax = 0
Pmax = 0
Id = 0
Rs_var = DoubleVar()
Vpsat_var = DoubleVar()
Vssat_var = DoubleVar()
Rd_var = DoubleVar()
Rc_var = DoubleVar()
Rb_var = DoubleVar()
Ra_var = DoubleVar()

# read initial values from defaults file

def fetch_defaults():
  global Ra, Rb, Rc, Rd, Rs, Vpsat, Vssat
  # reload defaults
  try:
    fp = open('uc3906-defaults.txt', 'r')
    # skip past comment line
    line = fp.readline()
    line = fp.readline()
    (Ra, Rb, Rc, Rd, Rs, Vpsat, Vssat) = line.split(' ')
    Ra_var.set(Ra)
    Rb_var.set(Rb)
    Rc_var.set(Rc)
    Rd_var.set(Rd)
    Rs_var.set(Rs)
    Vpsat_var.set(Vpsat)
    Vssat_var.set(Vssat)
    fp.close()
    return
  except IOError:
    # Defaults file doesn't exist, just go with default values already assigned
    print "No defaults file? Defaults stay in place"
    return

def press_print():
  global Ra, Rb, Rc, Rd, Rs, Vpsat, Vssat, Voc, Vf, Vt, V12, V31, Imax, Pmax, Ioct, Id, Tolerance_sequence, Tolerance_percent
  # TODO leading zeros!
  nowstring = str(datetime.now().year) + str(datetime.now().month) + str(datetime.now().day)
  # print parameters to a text file
  fp = open('/tmp/uc3906-' + nowstring + '.txt', 'w')
  fp.write('Id: {0:2.0f}\n'.format(Id))
  fp.close()
  return

def press_save():
  global Ra, Rb, Rc, Rd, Rs, Vpsat, Vssat
  # save parameters as new defaults
  fp = open('uc3906-defaults.txt', 'w')
  fp.write('# Ra Rb Rc Rd Rs Vpsat Vssat\n')
  fp.write(str(Ra)+ ' ')
  fp.write(str(Rb)+ ' ')
  fp.write(str(Rc)+ ' ')
  fp.write(str(Rd)+ ' ')
  fp.write(str(Rs)+ ' ')
  fp.write(str(Vpsat)+ ' ')
  fp.write(str(Vssat)+ '\n')
  fp.close()
  return

def range(expression, label):
  if(expression):
    status_label.config(text = 'ERROR: {:s} out of range!'.format(label))
  return

def parallelR(r1, r2):
  #print "r1: "
  #print r1
  #print "r2: "
  #print r2
  return (r1*r2)/(r1+r2)
  
def compute(sval):
  global Ra, Rb, Rc, Rd, Rs, Vpsat, Vssat, Voc, Vf, Vt, V12, V31, Imax, Pmax, Ioct, Id, Tolerance_sequence, Tolerance_percent

  if Tolerance_percent != 0:
    Ra_var.set(findNearestResistor(Ra_var.get(), Tolerance_sequence))
    Rb_var.set(findNearestResistor(Rb_var.get(), Tolerance_sequence))
    Rc_var.set(findNearestResistor(Rc_var.get(), Tolerance_sequence))
    Rd_var.set(findNearestResistor(Rd_var.get(), Tolerance_sequence))
    Rs_var.set(findNearestResistor(Rs_var.get(), Tolerance_sequence))

  Ra = Ra_var.get()
  Rb = Rb_var.get()
  Rc = Rc_var.get()
  Rd = Rd_var.get()
  Rs = Rs_var.get()
  Vpsat = Vpsat_var.get()
  Vssat = Vssat_var.get()


  Vf = ((Vref - Vpsat) * (Ra + Rb + Rc)) / Rc
  Voc = ((Vref - Vpsat) * (Ra + Rb + parallelR(Rc, Rd))) / parallelR(Rc, Rd)
  Vt = ((Vref - Vpsat) * (Ra + Rb + Rc)) / (Rb + Rc)
  V12 = 0.95 * Voc
  V31 = 0.9 * Vf
  Imax = 0.25 / Rs
  Pmax = (Imax * Imax) * Rs
  Ioct = 0.025 / Rs
  Id = (Vf / (Ra + Rb + Rb)) * 1000000.0

  #if (range(Id < 50,'Id') or range(Id > 100,'Id') or range(Voc > 14.5,'Voc') or 
  #	range(Voc < 14.0,'Voc') or range(Vf < 13.4,'Vf') or range(Vf > 13.8,'Vf')):
  status_label.config(text = "")
  range(Id < 50,'Id')
  range(Id > 100,'Id')

#    print(formatResistorValue(findNearestResistor(5000, sequence=e48)))
  # Update the displayed values
  Voc_label.config(text = 'Voc = {:5.3f}V'.format(Voc))
  Vf_label.config(text = 'Vf = {:5.3f}V'.format(Vf))
  Vt_label.config(text = 'Vt = {:5.3f}V'.format(Vt))
  V12_label.config(text = 'V state 1 -> state 2 = {:4.2f}V'.format(V12))
  V31_label.config(text = 'V state 3 -> state 1 = {:4.2f}V'.format(V31))
  Imax_label.config(text = 'Imax = {:4.2f}A'.format(Imax))
  Pmax_label.config(text = 'Pmax = {:4.2f}W'.format(Pmax))
  Ioct_label.config(text = 'Ioct state 2 -> state 3 = {:4.2f}A'.format(Ioct))
  Id_label.config(text = 'Id = {:2.0f}uA'.format(Id))

def press_reload():
  fetch_defaults()
  compute(0)
  return

def press_5_percent():
  global Tolerance_sequence, Tolerance_percent
  button_1_percent.config(background="#ffffff")
  button_5_percent.config(background="#00ff00")
  button_exact.config(background="#ffffff")
  Tolerance_sequence = e24
  Tolerance_percent = 5
  tolerance_label.config(text = 'Resistor tolerance 5%')
  compute(0)
  return

def press_1_percent():
  global Tolerance_sequence, Tolerance_percent
  button_1_percent.config(background="#00ff00")
  button_5_percent.config(background="#ffffff")
  button_exact.config(background="#ffffff")
  Tolerance_sequence = e96
  Tolerance_percent = 1
  tolerance_label.config(text = 'Resistor tolerance 1%')
  compute(0)
  return

def press_exact():
  global Tolerance_sequence, Tolerance_percent
  button_1_percent.config(background="#ffffff")
  button_5_percent.config(background="#ffffff")
  button_exact.config(background="#00ff00")
  Tolerance_sequence = None
  Tolerance_percent = 0
  tolerance_label.config(text = 'Exact resistor values')
  compute(0)
  return

#### "Main"

# Build the GUI

fetch_defaults()

# Slider to adjust Rs

Rs_var.set(Rs)
Rs_scale = Scale( root, background="#ffffff", command=compute, length = scale_length, variable = Rs_var, resolution=.001, from_=0.05, to=0.50, label="Rs" )
Rs_scale.pack(side=RIGHT)

# Slider to adjust Vpsat

Vpsat_var.set(Vpsat)
Vpsat_scale = Scale( root, background="#ffffff", command=compute, length = scale_length, variable = Vpsat_var, resolution=.001, from_=0.000, to=0.100, label="Vpsat" )
Vpsat_scale.pack(side=RIGHT)

# Slider to adjust Vssat

Vssat_var.set(Vssat)
Vssat_scale = Scale( root, background="#ffffff", command=compute, length = scale_length, variable = Vssat_var, resolution=.001, from_=0.000, to=0.100, label="Vssat" )
Vssat_scale.pack(side=RIGHT)

# Ditto Rd

Rd_var.set(Rd)
Rd_scale = Scale( root, background="#ffffff", command=compute, length = scale_length, variable = Rd_var, from_=1000000, to=300000, label="Rd" )
Rd_scale.pack(side=RIGHT)

# Ditto Rc

Rc_var.set(Rc)
Rc_scale = Scale( root, background="#ffffff", command=compute, length = scale_length, variable = Rc_var, from_=200000, to=20000, label="Rc" )
Rc_scale.pack(side=RIGHT)

# Ditto Rb

Rb_var.set(Rb)
Rb_scale = Scale( root, background="#ffffff", command=compute, length = scale_length, variable = Rb_var, from_=40000, to=9000, label="Rb" )
Rb_scale.pack(side=RIGHT)

# Ditto Ra

Ra_var.set(Ra)
# Create scale (slider) to adjust Ra
Ra_scale = Scale( root, background="#ffffff", command=compute, length = scale_length, variable = Ra_var, from_=240000, to=60000, label="Ra" )
# Connect the scale widget to the GUI
Ra_scale.pack(side=RIGHT)

# The labels for displaying values
Header_label = Label(root, background="yellow", foreground="blue")
Header_label.pack()
Header_label.config(text="UC3906 Dual Stage Modeler r0.9")

button_print = Button(root, background="#ffffff", text="Print parameters to /tmp/uc3906-design-yyyymmdd.txt", command=press_print)
button_print.pack(anchor="w")

button_reload = Button(root, background="#ffffff", text="Reload defaults from ./uc3906-defaults.txt", command=press_reload)
button_reload.pack(anchor="w")

button_save = Button(root, background="#ffffff", text="Save parameters as defaults in ./uc3906-defaults.txt", command=press_save)
button_save.pack(anchor="w")

button_5_percent = Button(root, background="#ffffff", text="Snap to 5% resistors", command=press_5_percent)
button_5_percent.pack(anchor="w")

button_1_percent = Button(root, background="#ffffff", text="Snap to 1% resistors", command=press_1_percent)
button_1_percent.pack(anchor="w")

button_exact = Button(root, background="#00ff00", text="Exact resistor values", command=press_exact)
button_exact.pack(anchor="w")

# The labels for displaying values
Voc_label = Label(root)
Voc_label.pack(anchor="w")

Vf_label = Label(root)
Vf_label.pack(anchor="w")

Vt_label = Label(root)
Vt_label.pack(anchor="w")

V12_label = Label(root)
V12_label.pack(anchor="w")

V31_label = Label(root)
V31_label.pack(anchor="w")

Imax_label = Label(root)
Imax_label.pack(anchor="w")

Pmax_label = Label(root)
Pmax_label.pack(anchor="w")

Ioct_label = Label(root)
Ioct_label.pack(anchor="w")

Id_label = Label(root)
Id_label.pack(anchor="w")

status_label = Label(root)
status_label.pack(anchor="w")

tolerance_label = Label(root)
tolerance_label.pack(anchor="w")
tolerance_label.config(text = "Exact resistor values")

# Load parameters from default


# Run the GUI event loop

root.mainloop()
##########
## END Modeler
##########
