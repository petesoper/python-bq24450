#!/usr/bin/python
itaper_max = 2.9
itaper_min = 0.0
itaper_inc = 0.01
#
# BQ24450/UC3906 battery charger design tool
#
# Sliders are used to set charger circuit parameters and the required resistor
# values are computed. The design equations are derived from the BQ24450 
# datasheet here:
#   http://www.ti.com/lit/ds/symlink/bq24450.pdf
#
# Pete Soper
# May, 2016
#
__author__ = "Pete Soper"
__license__ = "MIT"
__version__ = "0.50"
# Change history
#  5/29/2016 - Initial version escaping into the wild
#
# Variables names used in the compute routine track the datasheet symbols, 
# not Python conventions. The rest of the coding style is an absolute mess. 
#################
##############################################
## Resistor Utilities gotten from the Internet
##############################################
"""
A python script to calculate E96 resistor values

Originally published at techoverflow.net
"""
import itertools
import math

#__author__ = "Uli Koehler"
#__license__ = "CC0 1.0 Universal"
#__version__ = "1.1"

# Standard resistor sequences
e96 = [1.00, 1.02, 1.05, 1.07, 1.10, 1.13, 1.15, 1.18, 1.21, 1.24, 1.27, 1.30, 1.33, 1.37, 1.40,
       1.43, 1.47, 1.50, 1.54, 1.58, 1.62, 1.65, 1.69, 1.74, 1.78, 1.82, 1.87, 1.91, 1.96, 2.00,
       2.05, 2.10, 2.15, 2.21, 2.26, 2.32, 2.37, 2.43, 2.49, 2.55, 2.61, 2.67, 2.74, 2.80, 2.87,
       2.94, 3.01, 3.09, 3.16, 3.24, 3.32, 3.40, 3.48, 3.57, 3.65, 3.74, 3.83, 3.92, 4.02, 4.12,
       4.22, 4.32, 4.42, 4.53, 4.64, 4.75, 4.87, 4.99, 5.11, 5.23, 5.36, 5.49, 5.62, 5.76, 5.90,
       6.04, 6.19, 6.34, 6.49, 6.65, 6.81, 6.98, 7.15, 7.32, 7.50, 7.68, 7.87, 8.06, 8.25, 8.45,
       8.66, 8.87, 9.09, 9.31, 9.53, 9.76]
e48 = [1.00, 1.05, 1.10, 1.15, 1.21, 1.27, 1.33, 1.40, 1.47, 1.54, 1.62, 1.69,
       1.78, 1.87, 1.96, 2.05, 2.15, 2.26, 2.37, 2.49, 2.61, 2.74, 2.87, 3.01,
       3.16, 3.32, 3.48, 3.65, 3.83, 4.02, 4.22, 4.42, 4.64, 4.87, 5.11, 5.36,
       5.62, 5.90, 6.19, 6.49, 6.81, 7.15, 7.50, 7.87, 8.25, 8.66, 9.09, 9.53]
e24 = [1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.7, 3.0,
       3.3, 3.6, 3.9, 4.3, 4.7, 5.1, 5.6, 6.2, 6.8, 7.5, 8.2, 9.1]
e12 = [1.0, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2]


def getResistorRange(multiplicator, sequence=e96):
    "Get a single E96 range of resistors, e.g. for 1k use multiplicator = 1000"
    return [val*multiplicator for val in sequence]

def getStandardResistors(minExp=-1, maxExp=9, sequence=e96):
    "Get a list of all standard resistor values from 100mOhm up to 976 MOhm in Ohms"
    exponents = list(itertools.islice(itertools.count(minExp, 1), 0, maxExp - minExp))
    multiplicators = [10**x for x in exponents]
    return itertools.chain(*[getResistorRange(r, sequence=sequence) for r in multiplicators])

def findNearestResistor(value, sequence=e96):
    "Find the standard reistor value with the minimal difference to the given value"
    return min(getStandardResistors(sequence=sequence), key=lambda r: abs(value - r))

def _formatWithSuffix(v, suffix):
    """
    Format a given value with a given suffix.
    This helper function formats the value to 3 visible digits.
    v must be pre-multiplied by the factor implied by the suffix

    >>> _formatWithSuffix(1.01, "A")
    '1.01 A'
    >>> _formatWithSuffix(1, "A")
    '1.00 A'
    >>> _formatWithSuffix(101, "A")
    '101 A'
    >>> _formatWithSuffix(99.9, "A")
    '99.9 A'
    """
    if v < 10:
        res = "%.2f" % v
    elif v < 100:
        res = "%.1f" % v
    else:  # Should only happen if v < 1000
        res = "%d" % int(v)
    #Avoid appending whitespace if there is no suffix
    if suffix:
        return "%s %s" % (res, suffix)
    else:
        return res

def formatValue(v, unit=""):
    """
    Format v using SI suffices with optional units.
    Produces a string with 3 visible digits.

    >>> formatValue(1.0e-15, "V")
    '1.00 fV'
    >>> formatValue(234.6789e-3, "V")
    '234 mV'
    >>> formatValue(234.6789, "V")
    '234 V'
    >>> formatValue(2345.6789, "V")
    '2.35 kV'
    >>> formatValue(2345.6789e6, "V")
    '2.35 GV'
    >>> formatValue(2345.6789e12, "V")
    '2.35 EV'
    """
    suffixMap = {
        -5: "f", -4: "p", -3: "n", -2: "N<", -1: "m",
        0: "", 1: "k", 2: "M", 3: "G", 4: "T", 5: "E"
    }
    #Suffix map is indexed by one third of the decadic logarithm.
    exp = 0 if v == 0.0 else math.log(v, 10.0)
    suffixMapIdx = int(math.floor(exp / 3.0))
    #Ensure we're in range
    if suffixMapIdx < -5:
        suffixMapIdx = -5
    elif suffixMapIdx > 5:
        suffixMapIdx = 5
    #Pre-multiply the value
    v = v * (10.0 ** -(suffixMapIdx * 3))
    #Delegate the rest of the task to the helper
    return _formatWithSuffix(v, suffixMap[suffixMapIdx] + unit)


def formatResistorValue(v):
    return formatValue(v, unit="ohm")

#################
## End Resistor Utilities from other software
#################

# Short names to save typing

def fn(v):
    return findNearestResistor(v)

def nr(v):
    return " (" + formatResistorValue(findNearestResistor(v)) + ")"

def fr(v, s=e96):
    return formatResistorValue(v)

def fv(v,u):
    return formatValue(v,u)

from Tkinter import *
from datetime import datetime, date, time

use_saturation = False

id_initial = 50           # Divider current microamperes
ith_initial = 20	  # Trickle current milliamperes
vin_initial = 19.00       # Input voltage
voc_initial = 14.47       # Overcharge voltage
vf_initial = 13.65        # Float voltage
vth_initial = 10.5        # Minimum to reach with trickle current
imax_initial = 1.5     	  # Bulk charge max amperes
itaper_initial = imax_initial / 10.0 # Charge current at end of boost

vin_min = 16.00		  # Min input supply voltage
vin_max = 20.00		  # Max input supply voltage
led_current = 0.005	  # Max available current for driving LEDs
Vref = 2.30		  # Chip voltage reference
Vpsat = 0.028
Vssat = 0.003
Vpre = 2.5		  # Voltage drop across Q5 and its reverse diode
Hfe = 1000		  # Minimum gain of pass transistor (TIP147)
Vlim = 0.25		  # Voltage across main shunt Risns1 at Imax
Vtaper = 0.025		  # Voltage across taper current shunt Risns2 at Itaper
Vdext = 0.5		  # Forward drop of external diode

# Both the main divider current sink (Q7) and the boost current sink (Q8) are
# open collectors that are above ground, although for reasons I don't
# understand Q8's collector is just barely above ground. But if this isn't 
# taken into account the divider resistor values will be wrong by quite a bit.

#Q7_drop = 0.331		  # Measured value
Q7_drop = 0		  # Measured value
#Q8_drop = 0.017		  # Measured value
Q8_drop = 0		  # Measured value

Ra = Rb = Rc = Rd = Rsins1 = Rsins2 = Rth = Rp = Ptmax = 0.0
Id = Ith = Vin = Voc = Vf = Vth = Imax = Itaper = Pmax1 = Pmax2 = 0.0

scale_size = 500

root = Tk()
root.config(background="#ffff00")

# Compute new resistor values from the current parameter values.
# Called each time a slider is moved or button pushed
# (sval is artifact of tying the scale widgets to this function. It isn't used)

def compute(sval):
  global Id, Ith, Imax, Itaper, Vin, Vf, Voc, Vth, Rsins1, Rsins2, use_saturation
  global Rt, Rc, Rb, Ra, Rd, Rp, Pmax1, Pmax2, Ptmax

  # Divider current in normal (float or overcharge/boost/equalize) mode. Not
  # valid during trickle.
  Id = id_var.get() / 1000000.0

  # Trickle current in amps
  Ith = ith_var.get() / 1000.0

  # Battery voltage that must be reached to switch from precharge/trickle
  # to bulk charge

  Vth = vth_var.get()

  # Max charge current allowed during bulk charge
  Imax = imax_var.get()

  # Min charge current maintaining boost
  Itaper = itaper_var.get()

  # Charger input voltage
  Vin = vin_var.get()

  # Max pass transistor power
  Ptmax = (Vin - Vth - Vdext) * Imax
  
  # Charger battery float voltage
  Vf = vf_var.get() - Q7_drop 

  # Charger battery overcharge/boost/equalize voltage
  Voc = voc_var.get() - Q8_drop

  # Shunt resistor value is simply shunt voltage related to max current
  Rsins1 = Vlim / Imax

  # Max power

  Pmax1 = Vlim * Imax

  # For taper limit current the second shunt is measured

  Rsins2 = (Vtaper - (Itaper * Rsins1)) / Itaper

  # Max power

  Pmax2 = Vtaper * Itaper

  # The trickle charge resistor value is what is necessary to pass the trickle
  # current into the battery when it is at or below minimum voltage Vth. The
  # voltage drop across trickle charge transistor Q5 and its blocking diode
  # is given by the UC3906 datasheet as 2.5 volts and this is Vpre.

  Rt = (Vin - Vth - Vpre) / Ith

  # The bottom divider resistor during float charge is, by definition, going
  # to pass the divider design current when the reference voltage is on the
  # VFB pin
 
  if use_saturation:
    Vref_actual = Vref - Vpsat
  else:
    Vref_actual = Vref

  Rc = (Vref_actual) / Id

  # The top of the divider, Ra + Rb, is by definition going to pass the divider
  # current at the float voltage minus the reference voltage.

  Rsum = (Vf - (Vref_actual)) / Id

  # The middle divider resistor Rb is going to put the reference voltage on 
  # the CE pin which governs whether the precharge/trickle charge current is
  # enabled

  Rb = (((Vref_actual) / Vth) * (Rsum + Rc)) - Rc

  # The top divider resistor is just the remainder
  Ra = Rsum - Rb

  # When Q8 is on Rd is going to sink current in parallel with Rc, creating 
  # a lower resistance to force the charging voltage to the 
  # overcharge/equalize/boost level. Compute that resistance.

  Rx = (Vref_actual) / ((Voc - (Vref_actual)) / Rsum)

  # Now Rd is the resistance, which when in parallel with Rc, gives Rx

  Rd = parallelGetR2(Rx, Rc)

  # The pass transistor base current limiting resistor 

  Rp = ((Vin - 0.7) / Imax) * Hfe

  # Update the displayed values
  rc_label.config(text = "Rc = " + "{0:7.0f}".format(Rc) + nr(Rc), background="#ffff00")
  rd_label.config(text = "Rd = " + "{0:7.0f}".format(Rd) + nr(Rd), background="#ffff00")
  rb_label.config(text = "Rb = " + "{0:7.0f}".format(Rb) + nr(Rb), background="#ffff00")
  ra_label.config(text = "Ra = " + "{0:7.0f}".format(Ra) + nr(Ra), background="#ffff00")
  risns1_label.config(text = "Rsins1 = " + "{0:5.3f}(".format(Rsins1) + nr(Rsins1), background="#ffff00")
  risns2_label.config(text = "Rsins2 = " + "{0:5.3f}".format(Rsins2) + nr(Rsins2), background="#ffff00")
  rth_label.config(text = "Rt = " + "{0:7.0f}".format(Rt) + nr(Rt), background="#ffff00")
  rp_label.config(text = "Rp = " + "{0:7.0f}".format(Rp) + nr(Rp), background="#ffff00")
  pmax1_label.config(text = 'Rsins1 max power: %s' % fv(Pmax1, 'watt'), background="#ffff00")
  pmax2_label.config(text = 'Rsins2 max power: %s' % fv(Pmax2, 'watt'), background="#ffff00")
  ptmax_label.config(text = 'Pass transistor max power: %s' % fv(Ptmax, 'watt'), background="#ffff00")
  led_label.config(text = 'Indicator LED current limit resistor: %s' % fr((vin_max - 2.0) / led_current, e24), background="#ffff00")

# Build the GUI

def makeScale(initial_value, from_value, to_value, res_value, label_value):
  v = DoubleVar()
  v.set(initial_value)
  s = Scale(root, command=compute, length=scale_size, variable = v, from_=from_value, to=to_value, resolution = res_value, label = label_value, background="#ffff00", foreground="#0000ff", troughcolor="#0000ff", relief=RIDGE)
  s.pack(side=RIGHT)
  return v

# The slider widgets for the parameters. Each time the slider is moved the
# command function is called. The order of declaration and packing defines
# their positions in the GUI panel.

id_var = makeScale(id_initial, 100, 45, 1, "Id")
ith_var = makeScale(ith_initial, 40, 1, 1, "Ith")
imax_var = makeScale(imax_initial, 3.0, 0.25, 0.01, "Imax")
vin_var = makeScale(vin_initial, vin_max, vin_min, 0.25, "Vin")
voc_var = makeScale(voc_initial, 16.0, 6.0, 0.01, "Voc")
vth_var = makeScale(vth_initial, 11.5, 4.00, 0.05, "Vth")
vf_var = makeScale(vf_initial, 13.8, 5.0, 0.01, "Vf")
itaper_var = makeScale(itaper_initial, itaper_max, itaper_min, itaper_inc, "Itaper")

def makeLabel(text_value):
  v = Label(root)
  v.pack(anchor="w")
  v.config(text=text_value, background="#ffff00", foreground="#0000ff")
  return v

header_label1 = makeLabel("BQ24450/UC3906 Dual State Charger Designer " + __version__)
header_label2 = makeLabel("Reflects figure 10 and 8b within 9 here:")
header_label3 = makeLabel("http://www.ti.com/lit/ds/symlink/bq24450.pdf")
ra_label = makeLabel("")
rb_label = makeLabel("")
rc_label = makeLabel("")
rd_label = makeLabel("")
risns1_label = makeLabel("")
risns2_label = makeLabel("")
rp_label = makeLabel("")
rth_label = makeLabel("")
pmax1_label = makeLabel("")
pmax2_label = makeLabel("")
ptmax_label = makeLabel("")
led_label = makeLabel("")

# Find a second resistor value which, in parallel with the first gives
# the desired value Rx

def parallelGetR2(Rx, R1):
  return (R1 * Rx) / abs(Rx - R1)

# Toggle inclusion of transistor saturation voltage

def saturation_button_press():
  global use_saturation
  use_saturation = not use_saturation
  compute(0)

# /tmp/bq24450-YYYYMMDD.txt

def print_button_press():

  # TODO leading zeros!
  nowstring = str(datetime.now().year) + str(datetime.now().month) + str(datetime.now().day)

  print_button.config(background="#00ffff")
  # print parameters to a text file
  fp = open('/tmp/bq24450-' + nowstring + '.txt', 'w')
  fp.write('Id: %5.2f\n' % (Id * 1000000))
  fp.write('Imax: %5.2f\n' % Imax)
  fp.write('Itaper: %5.2f\n' % Itaper)
  fp.write('Vin: %5.2f\n' % Vin)
  fp.write('Voc: %5.2f\n' % Voc)
  fp.write('Vf: %5.2f\n' % Vf)
  fp.write('Vth: %5.2f\n' % Vth)
  fp.write('Ra: %5.0f (%5.0f)\n' % (Ra, fn(Ra)))
  fp.write('Rb: %6.0f (%6.0f)\n' % (Rb, fn(Rb)))
  fp.write('Rc: %6.0f (%6.0f)\n' % (Rc, fn(Rc)))
  fp.write('Rd: %6.0f (%6.0f)\n' % (Rd, fn(Rd)))
  fp.write('Rsins1: %5.3f (%5.3f)\n' % (Rsins1, fn(Rsins1)))
  if (Rsins2 == 0):
    fp.write('Rsins2:      0 (     0)\n' % (Rsins2, fn(Rsins2)))
  else:
    fp.write('Rsins2: %5.3f (%5.3f)\n' % (Rsins2, fn(Rsins2)))
  fp.write('Rth: %6.0f (%6.0f)\n' % (Rth, fn(Rth)))
  fp.write('Rp: %6.0f (%6.0f)\n' % (Rp, fn(Rp)))
  fp.write('Rsins1 max power: %s\n' % fv(Pmax1, 'watt'))
  fp.write('Rsins2 max power: %s\n' % fv(Pmax2, 'watt'))
  fp.write('Pass transistor max power: %s\n' % fv(Ptmax, 'watt'))
  fp.write('Indicator LED current limit resistor: %s\n' % fr((vin_max - 2.0) / led_current, e24))
  fp.close()
  return

# The control buttons

print_button = Button(root, background="#ffff00", text="Print details to /tmp/bq-YYYYMMDD.txt", command=print_button_press)
print_button.pack(anchor="w")

saturation_button = Button(root, background="#ffff00", text="Toggle inclusion of transistor saturation", command=saturation_button_press)
saturation_button.pack(anchor="w")

compute(0)

# Run the GUI event loop

root.mainloop()

#import inspect
#inspect.getmembers(inspect) 
#inspect.getfile(inspect)
#inspect.getmoduleinfo(inspect.getfile(inspect))
