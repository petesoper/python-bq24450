# bq24450
Python code for designing and modeling TI BQ24450-based lead acid battery chargers.

##TI BQ24450 Battery Charger Tools

Here are two Python programs for modeling and designing chargers based on the TI BQ24450 lead acid battery charger IC. This is pre-alpha software that isn't fully tested, with ad hoc style leveraging datasheet parameters over Python guidelines.

* bq24450-designer provides a graphical "what if?" that maps design parameters into the components necessary to effect the desired behaviors. That is, if the designer wants a charge voltage of 14.4 volts, float of 13.5, max bulk charge current of 1.5 amperes, etc, this program uses datasheet equations to calculate the necessary component values to match the specification.
* bq24450-modeler provides a graphical "what if?" that maps outboard component values to charger behavior. That is, if the designer knows the values of the divider network resistors for a charger implementation the modeler calculates parameters such as peak charge current, float voltage, trickle current, etc.

An additional Python program that might be of interest is a dual stage charger emulation using a Siglent programmable power supply. This (also pre-alpha!) software is in a separate repo for spd3303-related software at http://github.com/petesoper/python-spd3303 .

####Attribution
* This code includes one or more copies of standard resistor values and management functions written by Uli Koehler and published at https://techoverflow.net with CCO 1.0 license.
* The BQ24450 datasheet can be found at http://www.ti.com/lit/ds/symlink/bq24450.pdf

###Future Plans

1. Switch to standard Python coding style while preserving some aspects of datasheet parameter and component names.
2. Abstract out the design/modeling equations as side effect free functions.
3. Convert the GUI from coyote-ugly to utilitarian.

# Test
![cost = n + n (s / d+1)][metadata-formula1]
